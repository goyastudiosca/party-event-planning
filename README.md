Goya Studios Sound Stage

1541 N Cahuenga Blvd
Los Angeles, CA 90028
323-230-9005
United States
info@goyastudios.com

Visit Our Website: https://goyastudios.com/

Goya Studios provides special event space and flexible and expandable Hollywood sound stage options, complete with private parking, VIP lounges, green rooms, production offices, kitchens, private restrooms, and more.

Business Hours: Monday - Friday: 08:00 AM – 07:00 PM, Saturday  - Sunday: Closed